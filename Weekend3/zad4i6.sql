-- Zad 4.1

SELECT w.jezyk, date_part('day', avg(w.data_utworzenia - sp.data_wyjazdu)) as sredni_czas
  FROM wnioski w
  LEFT JOIN podroze p ON w.id = p.id_wniosku
  LEFT JOIN szczegoly_podrozy sp ON p.id = sp.id_podrozy
  GROUP BY jezyk;

-- Zad 4.2

SELECT count(
    CASE WHEN date_part('day', w.data_utworzenia - sp.data_wyjazdu) > 3*365 THEN w.id END)
  FROM wnioski w
  LEFT JOIN podroze p ON w.id = p.id_wniosku
  LEFT JOIN szczegoly_podrozy sp ON p.id = sp.id_podrozy;

SELECT count(
    CASE WHEN DATE_PART('year', w.data_utworzenia) - DATE_PART('year', sp.data_wyjazdu)  > 3 THEN w.id END)
  FROM wnioski w
  LEFT JOIN podroze p ON w.id = p.id_wniosku
  LEFT JOIN szczegoly_podrozy sp ON p.id = sp.id_podrozy;
COMMENT ON table wnioski IS '';

-- Zad 4.3

SELECT sum(liczba_pasazerow) as ile_klientow
FROM wnioski
WHERE typ_podrozy = 'biznesowy';

SELECT count(id) as ile_wyplat
FROM wnioski
WHERE stan_wniosku = 'wyplacony' AND typ_podrozy = 'biznesowy';

-- Zad 4.4

SELECT date_part('day', avg(w.data_utworzenia - s2.data_wyjazdu)) as sredni_czas
FROM wnioski w
  JOIN podroze p ON w.id = p.id_wniosku
  JOIN szczegoly_podrozy s2 ON p.id = s2.id_podrozy
WHERE typ_podrozy = 'biznesowy';

-- Zad 6.1

select id_wniosku
from analiza_prawna
union all
  select id_wniosku
  from analiza_operatora;

select id_wniosku
from analiza_prawna
EXCEPT
  select id_wniosku
  from analiza_operatora;

select id_wniosku
from analiza_prawna
INTERSECT
  select id_wniosku
  from analiza_operatora;


select row_number() over (partition by partner order by data_utworzenia), *
from wnioski
where date_part('year', data_utworzenia) = '2018';

select row_number() over (PARTITION BY date_part('year', data_utworzenia)), *
from wnioski w
join analiza_prawna ap on w.id = ap.id_wniosku
where agent_id is not null;


-- Zad 7


select
  date_part('month', data_utworzenia) as miesiac_utworzenia,
  count(1) as biezacy_miesiac,
  lag(count(1)) over () as poprzedni_miesiac,  -- lag wstawia się od razu po kolumnie którą chce się zlagować
  (count(1) - lag(count(1)) over ()) / lag(count(1)) over()::numeric as zmiana
from wnioski
where date_part('year', data_utworzenia) = '2017'
group by date_part('month', data_utworzenia);


-- Zad 9


select
  partner,
  unnest(
      percentile_disc(ARRAY [0.25, 0.5, 0.75])
      WITHIN GROUP (ORDER BY kwota_rekompensaty)) AS percentyle_kwota_finalna,
  unnest(
      percentile_disc(ARRAY [0.25, 0.5, 0.75])
      WITHIN GROUP (ORDER BY kwota_rekompensaty_oryginalna)) AS percentyle_kwota_oryginalna,
  unnest(
     ARRAY [0.25, 0.5, 0.75]
  ) as rzad_percentylu
from wnioski
group by partner;


-- Zad 10

SELECT partner, corr(kwota_rekompensaty, liczba_pasazerow) as kor_rek,
  corr(kwota_rekompensaty_oryginalna - wnioski.kwota_rekompensaty, liczba_pasazerow) as kor_rek_pas
FROM wnioski
WHERE
  kwota_rekompensaty IS NOT NULL AND
  kwota_rekompensaty_oryginalna IS NOT NULL AND
  liczba_pasazerow IS NOT NULL
GROUP BY partner;


-- Zad 11


SELECT
  row_number() over (PARTITION BY partner ORDER BY data_utworzenia) as r_no,
  id,
  partner,
  data_utworzenia,
  avg(kwota_rekompensaty) over (PARTITION BY partner) as srednia_dla_przewoznika,
  stddev(kwota_rekompensaty) over (PARTITION BY partner) as odchylenie_dla_przewoznika
FROM wnioski
WHERE stan_wniosku = 'wyplacony'
ORDER BY partner, r_no;

