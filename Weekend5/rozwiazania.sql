with tabela_lotow as (
  select w.id, sp.kod_wyjazdu, sp.kod_przyjazdu from wnioski w, podroze p, szczegoly_podrozy sp
  where w.id=p.id_wniosku and p.id= sp.id_podrozy
)
SELECT kod_wyjazdu, kod_przyjazdu, count(1) from tabela_lotow
group by (kod_przyjazdu, kod_wyjazdu)
order by 3 desc;


-- Zad 3

select
  region,
--   sum((replace(population, ',' , '')::INTEGER)),
  avg(life) as srednia_dlugosc_zycia,
  avg(income) as sredni_zarobek
from gapminder
where year = 2015
group by region;

select
  year,
  life,
  income,
  (income - lag(income) OVER()) / income * 100.0 as yoy_income
from gapminder
where country ILIKE 'poland'
order by year;