-- Zad 1
select
  row_number() over (order by data_utworzenia),
  id,
  avg(kwota_rekompensaty) over (PARTITION BY powod_operatora)
from wnioski
where date_part('year', data_utworzenia) = '2015' and stan_wniosku = 'wyplacony'
order by data_utworzenia;

-- Zad 2
SELECT
  partner,
  count(1) as ile_wnioskow,
  percentile_disc(0.5) within group (order by kwota_rekompensaty) as mediana_rekompensaty,
  min(kwota_rekompensaty) as min_rekompensaty,
  max(kwota_rekompensaty) as max_rekompensaty
from wnioski
group by partner;

-- Zad 3
with info_miesiac_rok as (
  select
    to_char(data_utworzenia, 'YYYY-MM') as mies_rok,
    count(1) ile_wnioskow
  from wnioski
  group by to_char(data_utworzenia, 'YYYY-MM')
)

select * from info_miesiac_rok
union
select 'wszystkie', count(1) from wnioski;

-- Zad 4
with wnioski_abstrakt as (
  select w.id, stan_wniosku, s2.identyfikator_podrozy
  from wnioski w
  join podroze p ON w.id = p.id_wniosku
  join szczegoly_podrozy s2 ON p.id = s2.id_podrozy
),

nowe_wnioski as (
  select * from wnioski_abstrakt
  where
    stan_wniosku = 'nowy' and
    identyfikator_podrozy not like '%----'
),

stare_wnioski as (
  select * from wnioski_abstrakt
  where
    stan_wniosku = 'wyplacony' and
    identyfikator_podrozy not like '%----'
)


select *
from nowe_wnioski nw
join stare_wnioski sw on nw.identyfikator_podrozy = sw.identyfikator_podrozy
order by nw.identyfikator_podrozy;