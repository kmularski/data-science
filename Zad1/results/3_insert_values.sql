INSERT INTO doctors (id, first_name, last_name, sex, city, voivodeship, country, street, street_prefix, date_birth, pesel) VALUES
  (1, 'doctor1', 'Kowalski', FALSE , 'Gdańsk', 'Pomorskie', 'Polska', 'Starowiejska', 'Ul.', to_date('1994 05 11', 'YYYY MM DD'), '92051514933');
INSERT INTO doctors (id, first_name, last_name, sex, city, voivodeship, country, street, street_prefix, date_birth, pesel) VALUES
  (2, 'doctor2', 'Nowak', FALSE, 'Wrocław', 'Śląskie', 'Polska', 'Śląska', 'Ul.', to_date('1994 05 11', 'YYYY MM DD'), '92011123186');
INSERT INTO doctors (id, first_name, last_name, sex, city, voivodeship, country, street, street_prefix, date_birth, pesel) VALUES
  (3, 'doctor3', 'Abramovic', TRUE, 'Praga', 'Praga', 'Czechy', 'Czeska', 'Al.', to_date('1994 05 11', 'YYYY MM DD'), '48033059419');

INSERT INTO patients (id, first_name, last_name, sex, city, voivodeship, country, street, street_prefix, first_registered, date_birth, pesel, doctor_id) VALUES
  (1, 'Karol', 'Mularski', FALSE , 'Gdynia', 'Pomorskie', 'Polska', 'Unruga', 'Ul.', to_date('2015 06 21', 'YYYY MM DD'), to_date('1994 05 11', 'YYYY MM DD'), '94051111936', 1);
INSERT INTO patients (id, first_name, last_name, sex, city, voivodeship, country, street, street_prefix, first_registered, date_birth, pesel, doctor_id) VALUES
  (2, 'Nikola', 'Tesla', FALSE , 'Manhattan', 'Nowy Jork', 'USA', '9000 Street', 'Ul.', to_date('1900 07 12', 'YYYY MM DD'), to_date('1856 07 07', 'YYYY MM DD'), '64031236171', 1);
INSERT INTO patients (id, first_name, last_name, sex, city, voivodeship, country, street, street_prefix, first_registered, date_birth, pesel, doctor_id) VALUES
  (3, 'Ava', 'Taylor', TRUE, 'Ottawa', 'Ontario', 'Kanada', 'Ontarian Street', 'Ul.', to_date('1995 07 12', 'YYYY MM DD'), to_date('2000 07 07', 'YYYY MM DD'), '95051951887', 3);

INSERT INTO drugs (id, name_latin, name, description, price, price_ref, typical_dosage) VALUES
  (1, 'Valium', 'Diazepan', 'A benzodiazepine with anticonvulsant, anxiolytic, sedative, muscle relaxant, and amnesic properties and a long duration of action. Its actions are mediated by enhancement of GAMMA-AMINOBUTYRIC ACID activity. ',
  39, 19, 'One pill per day, before breakfast');
INSERT INTO drugs (id, name_latin, name, description, price, price_ref, typical_dosage) VALUES
  (2, NULL , 'Vanillic acid', 'A flavoring agent. It is the intermediate product in the two-step bioconversion of ferulic acid to vanillin. (J Biotechnol 1996;50(2-3):107-13)',
  109, 39, 'One scoop per day, any part of the day');
INSERT INTO drugs (id, name_latin, name, description, price, price_ref, typical_dosage) VALUES
  (3, 'Letermovir', NULL, 'Has antiviral activity. ', 49, 9, 'Twice a day, in the morning and in the evening - two tablespoons during meal');

INSERT INTO visits (id, patient_id, doctor_id, status, datetime) VALUES (
  1, 1, 1, TRUE, to_timestamp('2015 01 02 10 15', 'YYYY MM DD HH24 MI')
);
INSERT INTO visits (id, patient_id, doctor_id, status, datetime) VALUES (
  2, 3, 3, TRUE, to_timestamp('2016 11 07 10 30', 'YYYY MM DD HH24 MI')
);
INSERT INTO visits (id, patient_id, doctor_id, status, datetime) VALUES (
  3, 2, 1, FALSE , to_timestamp('2019 02 03 20 15', 'YYYY MM DD HH24 MI')
);

INSERT INTO visit_drug (id, drug_id, visit_id, amount, dosage) VALUES (
    1, 1, 1, 2, 'Jak na ulotce'
);

INSERT INTO visit_drug (id, drug_id, visit_id, amount, dosage) VALUES (
    2, 3, 1, 1, 'Całe opakowanie połknąć z opakowaniem'
);

INSERT INTO visit_drug (id, drug_id, visit_id, amount, dosage) VALUES (
    3, 2, 2, 3, 'Zakroplić oczy dwa razy dziennie po 2 krople na oko przez 3 miesiące'
);