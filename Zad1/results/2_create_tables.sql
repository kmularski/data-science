CREATE TABLE IF NOT EXISTS doctors (
  id INTEGER PRIMARY KEY ,
  first_name VARCHAR(100),
  last_name VARCHAR(100),
  sex BOOLEAN,
  city VARCHAR(100),
  voivodeship VARCHAR(100),
  country VARCHAR(100),
  street VARCHAR(100),
  street_prefix VARCHAR(10),
  first_registered DATE,
  date_birth TIMESTAMP,
  pesel VARCHAR(11),
  facility_name VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS patients (
  id INTEGER PRIMARY KEY ,
  first_name VARCHAR(100),
  last_name VARCHAR(100),
  sex BOOLEAN,
  city VARCHAR(100),
  voivodeship VARCHAR(100),
  country VARCHAR(100),
  street VARCHAR(100),
  street_prefix VARCHAR(10),
  first_registered DATE,
  date_birth TIMESTAMP,
  pesel VARCHAR(11),
);

CREATE TABLE IF NOT EXISTS drugs (
  id INTEGER PRIMARY KEY ,
  name_latin VARCHAR(100),
  name VARCHAR(100),
  description TEXT,
  price INT,
  price_ref INT,
  typical_dosage TEXT
);

CREATE TABLE IF NOT EXISTS visits (
  id INTEGER PRIMARY KEY ,
  patient_id INTEGER,
  doctor_id INTEGER,
  status BOOLEAN DEFAULT NULL ,
  datetime TIMESTAMP,
  FOREIGN KEY (patient_id) REFERENCES patients,
  FOREIGN KEY (doctor_id) REFERENCES doctors
);


CREATE TABLE IF NOT EXISTS visit_drug (
  id INTEGER PRIMARY KEY ,
  drug_id INTEGER,
  visit_id INTEGER,
  amount INTEGER,
  dosage TEXT,
  refund BOOL DEFAULT FALSE
  FOREIGN KEY (drug_id) REFERENCES drugs,
  FOREIGN KEY (visit_id) REFERENCES visits
)