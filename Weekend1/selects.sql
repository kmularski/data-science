select ticker,
    max(maxprice) as max_price,
    min(minprice)as min_price,
    sum(volume) as volume_sum,
    avg(volume) as volume_avg,
    min(openprice - closeprice) as min_price_diff,
    max(openprice - closeprice) as max_price_diff,
    avg(openprice - closeprice) as avg_price_diff
from gpw
group by ticker
order by ticker;

select AVG(openprice - minprice) as avg_price_diff from gpw
    where volume > 1000 and volume < 1000000;
select min(openprice - minprice) as min_price_diff from gpw
    where ticker like 'A%';
select max(openprice - minprice) as max_price_diff from gpw
    where ticker like 'A%';

select * from gpw sort by volume asc;

CREATE OR REPLACE FUNCTION  get_best_long_position ()
RETURNS varchar as $best$
declare
        best varchar;
BEGIN
    select ticker into total where max(closeprice-openprice)
END; LANGUAGE plpgsql