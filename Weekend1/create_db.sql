DROP TABLE IF EXISTS order_product;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS products;

CREATE TABLE IF NOT EXISTS users (
  id INTEGER PRIMARY KEY ,
  first_name VARCHAR(255),
  last_name VARCHAR(255),
  age INTEGER,
  sex BOOLEAN,
  city VARCHAR(255),
  voivodeship VARCHAR(255),
  country VARCHAR(255),
  street VARCHAR(255),
  registered_at DATE,
  last_login DATE
);

CREATE TABLE IF NOT EXISTS products (
  id INTEGER PRIMARY KEY ,
  name VARCHAR(255),
  category VARCHAR(255),
  description TEXT,
  stock INTEGER,
  price INTEGER
);

CREATE TABLE IF NOT EXISTS orders (
  id INTEGER PRIMARY KEY ,
  date_bought DATE DEFAULT current_date,
  date_paid DATE DEFAULT NULL ,
  user_id INTEGER,
  FOREIGN KEY (user_id) REFERENCES users
);

CREATE TABLE IF NOT EXISTS order_product (
  id INTEGER PRIMARY KEY,
  order_id INTEGER,
  product_id INTEGER,
  FOREIGN KEY (product_id) REFERENCES products,
  FOREIGN KEY (order_id) REFERENCES orders,
  amount INTEGER
)