TRUNCATE TABLE products;

INSERT INTO products (id, name, category, description, stock, price)
VALUES (
    1,
  'Nożyk',
  'Nożyki',
  'Super nożyk',
  '39',
  '10'
);

INSERT INTO products (id, name, category, description, stock, price)
VALUES (
    2,
  'Wędka z turbo spustem',
  'Wędki',
  'Ryby same się łowią',
  '5',
  '1000'
);

INSERT INTO products (id, name, category, description, stock, price)
VALUES (
    3,
  'Siedzonko rozkładane',
  'Krzesła wędkarskie',
  'Do kompletu pasuje: wędka i alkohol',
  '100',
  '39'
);