TRUNCATE TABLE products;

INSERT INTO order_product (id, order_id, product_id, amount)
VALUES (
    1,
    1,
    1,
    1
);

INSERT INTO order_product (id, order_id, product_id, amount)
VALUES (
    2,
    2,
    1,
    1
);

INSERT INTO order_product (id, order_id, product_id, amount)
VALUES (
    3,
    3,
    3,
    100
);