TRUNCATE TABLE users;

INSERT INTO users (id,
                   first_name,
                   last_name,
                   age,
                   sex,
                   city,
                   voivodeship,
                   country,
                   street,
                   registered_at,
                   last_login)
VALUES (
  1,
  'Karol',
  'M',
  '20',
  '0',
  'Gdynia',
  'Pomorskie',
  'Polska',
  'Unruga 108',
  to_date('2000 05 11', 'YYYY MM DD'),
  to_date('2000 05 11', 'YYYY MM DD')
);

INSERT INTO users (id,
                   first_name,
                   last_name,
                   age,
                   sex,
                   city,
                   voivodeship,
                   country,
                   street,
                   registered_at,
                   last_login)
VALUES (
  2,
  'Nie-Karol',
  'Nie-M',
  '34',
  '0',
  'Gdańsk śmierdzi',
  'Pomorskie',
  'Polska',
  'Wiejska 8',
  to_date('2018 06 12', 'YYYY MM DD'),
  to_date('2018 06 12', 'YYYY MM DD')
);

INSERT INTO users (id,
                   first_name,
                   last_name,
                   age,
                   sex,
                   city,
                   voivodeship,
                   country,
                   street,
                   registered_at,
                   last_login)
VALUES (
  3,
  'Losowe_imie',
  'Losowe_nazwisko',
  '50',
  '1',
  'Boston',
  'Masaciusets',
  'USA',
  '1000 New street',
  to_date('2018 01 02', 'YYYY MM DD'),
  to_date('2018 01 02', 'YYYY MM DD')
);

