TRUNCATE TABLE products;

INSERT INTO orders (id, date_bought, date_paid, user_id)
VALUES (
    1,
  to_date('2000 05 11', 'YYYY MM DD'),
  to_date('2000 05 12', 'YYYY MM DD'),
  1
);

INSERT INTO orders (id, date_bought, date_paid, user_id)
VALUES (
    2,
  to_date('2018 06 11', 'YYYY MM DD'),
  to_date('2018 06 12', 'YYYY MM DD'),
  1
);

INSERT INTO orders (id, date_bought, date_paid, user_id)
VALUES (
    3,
  to_date('2007 05 11', 'YYYY MM DD'),
  to_date('2011 05 12', 'YYYY MM DD'),
  3
);